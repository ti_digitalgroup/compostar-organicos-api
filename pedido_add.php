<?php
    if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: POST, GET, DELETE, PUT, PATCH, OPTIONS');
        header('Access-Control-Allow-Headers: token, Content-Type');
        header('Access-Control-Max-Age: 1728000');
        header('Content-Length: 0');
        header('Content-Type: text/plain');
        die();
    }

    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: POST, GET, DELETE, PUT, PATCH, OPTIONS');
    header('Content-Type: application/json');

    $dados = json_decode(file_get_contents("php://input"));

    include('conexao.php');
    $pdo = Conectar();

    $stmt = $pdo->prepare('INSERT INTO dev_pedidos (nome, endereco, telefone, email, ecobag, valor_total, frete) VALUES (:nome, :endereco, :telefone, :email, :ecobag, :valor_total, :frete)');

    $stmt->bindParam(':nome', $dados->user->nome);
    $stmt->bindParam(':endereco', $dados->user->endereco);
    $stmt->bindParam(':telefone', $dados->user->telefone);
    $stmt->bindParam(':email', $dados->user->email);
    $stmt->bindParam(':ecobag', $dados->user->ecobag);
    $stmt->bindParam(':valor_total', $dados->total);
    $stmt->bindParam(':frete', $dados->frete);

    $stmt->execute();
    $id = $pdo->lastInsertId();

    $response = $stmt->rowCount();

    if($response){
        $response_array['status'] = 'success';
        $response_array['msg'] = 'Pedido adicionado com sucesso!';
        $response_array['id'] = $id;

        
        foreach($dados->produtos as $p){

            if($p->quantidade > 0){
                $stmt = $pdo->prepare('INSERT INTO dev_pedido_produtos (id_pedido, id_produto, quantidade, tipo) VALUES (:id_pedido, :id_produto, :quantidade, :tipo)');

                $stmt->bindParam(':id_pedido', $id);
                $stmt->bindParam(':id_produto', $p->id);
                $stmt->bindParam(':quantidade', $p->quantidade);
                $stmt->bindParam(':tipo', $p->escolha_tipo);
                
                $stmt->execute();
                $id_pedido_produto = $pdo->lastInsertId();

                $response2 = $stmt->rowCount();
                $response_array['pedido'] = $p->nome;
            }

            
        };



    } else {
        $response_array['status'] = 'error';
        $response_array['msg'] = 'Ops! Erro ao adicionar o pedido. Tente novamente.';
    }
    print json_encode($response_array);
?>