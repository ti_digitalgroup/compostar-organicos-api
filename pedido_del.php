<?php
    if ($_SERVER['REQUEST_METHOD'] === 'OPTIONS') {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: POST, GET, DELETE, PUT, PATCH, OPTIONS');
        header('Access-Control-Allow-Headers: token, Content-Type');
        header('Access-Control-Max-Age: 1728000');
        header('Content-Length: 0');
        header('Content-Type: text/plain');
        die();
    }

    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');

    include('conexao.php');
    $pdo = Conectar();

    $stmt = $pdo->prepare('DELETE FROM dev_pedidos WHERE id = :id');
    $stmt->bindParam(':id', $_GET['id']);

    $stmt->execute();
    $id = $pdo->lastInsertId();

    $response = $stmt->rowCount();


    $stmt = $pdo->prepare('DELETE FROM dev_pedido_produtos WHERE id_pedido = :id');
    $stmt->bindParam(':id', $_GET['id']);

    $stmt->execute();
    $id = $pdo->lastInsertId();

    if($response){
        $response_array['status'] = 'success';
        $response_array['msg'] = 'Pedido removido com sucesso!';
        $response_array['id'] = $id;

    } else {
        $response_array['status'] = 'error';
        $response_array['msg'] = 'Ops! Erro ao adicionar o pedido. Tente novamente.';
    }
    print json_encode($response_array);

?>