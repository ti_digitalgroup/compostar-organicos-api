<?php
	
	header('Content-type: application/json');
    
	include('conexao.php');
	$pdo = Conectar();

	$sql = "SELECT p.id, p.id_categoria, p.nome, p.valor_venda as valor, p.volume, p.tipo, p.escolha_tipo
	FROM dev_produtos p
	WHERE p.status = 1
	";
	$stm = $pdo->prepare($sql);
	$stm->execute();
	$result = $stm-> fetchAll(PDO::FETCH_ASSOC);
	echo json_encode($result);	
?>